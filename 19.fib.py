# 斐波那契数列
"""
字典查找法
设定一个空字典，将计算结果存入，当其他计算时候直接查字典
如果有改值则直接返回

效率：减少一半枝叶计算量


bottom-up dp algorithm  自下而上算法

fib = {}
-> for k in range(1, n+1):
    [    if k <=2:f=1                 ]
    [    else: f=fib[k-1]+fib[k-2]    ]
    fib[k] = f
    return fib[n]
"""


def bottom_up(n):
    fib = {}
    for k in range(1, n+1):
        if k <= 2:
            f = 1
        else:
            f = fib[k-1]+fib[k-2]
        fib[k] = f
    return fib[n]


if __name__ == '__main__':
    print(bottom_up(10000))
