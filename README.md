# Introduction to Algorithms Dynamic Programming 动态规划

## 项目介绍

> MIT 6.006算法导论，2011年秋
> This course provides an introduction to mathematical modeling of computational problems. It covers the common algorithms, algorithmic paradigms, and data structures used to solve these problems. The course emphasizes the relationship between algorithms and programming, and introduces basic performance measures and analysis techniques for these problems.
> [视频地址](https://www.youtube.com/playlist?list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb)
> [MIT课程地址](http://ocw.mit.edu/6-006F11)

## 动态规划1: 在斐波那契数列，最短路径中的应用
```

```


## 版本信息
* 单元8：高级主题
- 24 算法研究主题
- 23 计算复杂度
* 单元7：动态编程
- 22 两种猜测；钢琴/吉他指法，俄罗斯方块培训，超级马里奥兄弟
- 21 字符串子问题，伪多项式时间；括号，编辑距离，背包
- 20 父指针；文本对齐，完美信息二十一点
- 19 记忆，子问题，猜测，自下而上；斐波那契，最短路径[Memoization, subproblems, guessing, bottom-up; Fibonacci, shortest paths](https://www.youtube.com/watch?v=OQ5jsbhAv_M&list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb&index=19)
* 单元6：最短路径
- 18 加快迪克斯特拉
- 17 贝尔曼福特
- 16 迪克斯特拉
- 15 单源最短路径问题
* 单元5：图表
- 14 深度优先搜索（DFS），拓扑排序
- 13 广度优先搜索（BFS）
* 单元4：数值
- 12 平方根，牛顿法
- 11 整数运算，唐津乘法
* 单元3：散列
- 10 开放式寻址，加密哈希
- 9 表翻倍，Karp-Rabin
- 8 链式散列
* 单元2：排序和树
- 7 计算排序，基数排序，排序和搜索的下限
- 6 AVL树，AVL排序
- 5 二进制搜索树，BST排序
- 4 堆和堆排序
- 3 插入排序，合并排序
* 单元1：简介
- 2 计算模型，Python成本模型，文档距离
- 1 算法思维，发现峰值[Algorithmic thinking, peak finding](https://www.youtube.com/watch?v=HtSuA80QTyo&list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb)